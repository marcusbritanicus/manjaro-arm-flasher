# Manjaro ARM Flasher

This repo contains the source for the Manjaro ARM Flasher tool, written in PyQt5 (Python 3 and QT 5).


**WIP**

## Dependencies (Arch package names)
So far only:
* python-pyqt5
* python-blkinfo


## Usage
```
manjaro-arm-flasher
```

## Disclaimer
This application needs root permissions to flash the image to the block device.

The project contains a file (`manjaro-arm-flasher-permissions`), that can be placed in `/etc/sudoers.d/`. Doing so will allow the application to run with root rights without asking for the password!

## Known issues
Check the Issues section of this gitlab repo.
