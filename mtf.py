#!/usr/bin/python3

import os
import sys
import time

from PyQt5.QtCore import QThread, QCoreApplication, pyqtSignal

from bs4 import BeautifulSoup as BS
import requests
from typing import List
from typing import Tuple

burl = "https://osdn.net"

proxies = {
    "http": os.getenv( "HTTP_PROXY" ),
    "https": os.getenv( "HTTPS_PROXY" ),
}

__all__ = [
    "DirLister"
]

class DirLister( QThread ):
    """A remote directory lister.
     This class is inspired by mtf.py script that scrapes osdn.net to
     obtain the torrent files for manjaro devices.
    """

    imageFound = pyqtSignal( str )
    folderFound = pyqtSignal( str )

    def __init__( self, url = "", details = "all" ):
        super( DirLister, self ).__init__()

        self.fileUrls = []
        self.seen = []

        self.details = details[:]

        self.toBeWalked = [ url ]

    def start( self ):
        self.running = True
        QThread.start( self )

    def stop( self ):
        self.running = False
        self.waitForFinished( -1 )

    def __extractUrlFromTableRow( self, trrow ):
        """Extracts the url for the file from the table row, trrow."""
        url = None
        row = BS( str( trrow ), "lxml" )
        tds = row.find_all( "td" )
        for td in tds:
            atts = td.attrs
            if atts is not None and "class" in atts and atts["class"][0] == "name":
                link = td.find( "a" )
                if "( Parent folder )" != link.get_text().strip():
                    url = link["href"]
        return url

    def __extractUrlByClass( self, row ):
        """Extracts the url if the class is file or dir."""
        url = None
        xclass = None
        atts = row.attrs
        if atts is not None and "class" in atts:
            xclass = atts["class"][0]
            if xclass in ["file", "dir"]:
                url = self.__extractUrlFromTableRow( row )
        return ( url, xclass )

    def scrape( self, url ):

        r = requests.get( url, proxies = proxies )
        hpage = BS( r.text, "lxml" )
        rows = hpage.find_all( "tr" )

        if ( r.status_code != 200 ):
            return;

        for row in rows:
            if not self.running:
                break;

            xurl, xclass = self.__extractUrlByClass( row )

            if ( xurl == None ):
                continue;

            if xclass == "file":
                furl = burl[:]
                if xurl.startswith( "/" ):
                    furl = f"{furl}{xurl}"

                else:
                    furl = f"{furl}/{xurl}"

                if ( self.details == "all" or self.details == "files" ):
                    if ( furl.endswith( ".img.xz" ) ):
                        self.fileUrls.append( furl )
                        self.imageFound.emit( furl )
                        print( furl )

            elif xclass == "dir":
                yurl = ""
                if xurl.startswith( "/" ):
                    yurl = f"{burl}{xurl}"

                else:
                    yurl = f"{burl}/{xurl}"

                if len( yurl ) >= len( url ):
	                self.folderFound.emit( yurl )
	                print( yurl )

                if ( self.details == "all" ):
                    self.toBeWalked.append( yurl )

    def run( self ):

        while( len( self.toBeWalked ) ):
            if ( not self.running ):
                break

            url = self.toBeWalked.pop( 0 )
            if url in self.seen:
                continue

            self.seen.append( url )
            self.scrape( url )

        else:
            print( "... FIN!" )


if __name__ == "__main__":

    app = QCoreApplication( sys.argv )

    # Get the Editions of RPi 4
    walker2 = DirLister( url = burl + "/projects/manjaro-arm/storage/rpi4", details = "dirs" )
    walker2.start()

    # Get the Versions for KDE-Plasma
    walker3 = DirLister( url = burl + "/projects/manjaro-arm/storage/rpi4/kde-plasma", details = "dirs" )
    walker3.start()

    # Get the image url for KDE Plasma for RPi4
    walker4 = DirLister( url = burl + "/projects/manjaro-arm/storage/rpi4/kde-plasma/20.06", details = "files" )
    walker4.start()

    walker2.wait()
    walker3.wait()
    walker4.wait()
